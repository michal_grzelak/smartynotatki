USE [zbiletem-live]
GO

IF OBJECT_ID('[ZBILETEM-STATS]', 'V') IS NOT NULL
    DROP VIEW [ZBILETEM-STATS]
GO

CREATE VIEW [ZBILETEM-STATS] AS
SELECT PP.YEAR, PP.MONTH, STATS.SUM  AS 'SUM IN YEAR', PP.COUNT  AS 'COUNT PER MONTH', STATS.AVG AS 'AVERAGE PER MONTH', PP.COUNT - STATS.AVG AS 'STDDEV'
FROM (
		SELECT YEAR(P.TimeStared) AS [YEAR], MONTH(P.TimeStared) AS [MONTH], COUNT(1) AS [COUNT]
		FROM Purchase P
		GROUP BY YEAR(P.TimeStared), MONTH(P.TimeStared)
	) PP
OUTER APPLY (
		SELECT AVG(CNT.COUNT) AS AVG, SUM(CNT.COUNT) AS SUM
		FROM (
				SELECT COUNT(*) AS [COUNT]
				FROM Purchase P
				WHERE YEAR(P.TimeStared) = PP.YEAR
				GROUP BY MONTH(P.TimeStared)
			) CNT
	) STATS
GO

SELECT 'TICKET STATS' AS TITLE, *
FROM [ZBILETEM-STATS] STATS
ORDER BY STATS.YEAR, STATS.MONTH
GO

SELECT 'TICKET STATS WITH >0 STDDEV' AS TITLE, *, LEFT(CAST(ROUND(STATS.STDDEV / CAST(STATS.[AVERAGE PER MONTH] AS DECIMAL) * 100, 2) AS NVARCHAR), 2) + '%' AS [% MORE THAN AVG]
FROM [ZBILETEM-STATS] STATS
WHERE STATS.STDDEV > 0
ORDER BY STATS.YEAR, STATS.MONTH
GO

SELECT 'CLIENT STATS' AS TITLE, *
FROM (
		SELECT C.Id AS [CLIENT ID], COUNT(1) AS [BOUGHT TICKETS]
		FROM Client C
		JOIN Purchase P ON C.Id = P.ClientId
		GROUP BY C.Id
	) BT
OUTER APPLY (
		SELECT AVG(CAST(XX.[TICKET COUNT] AS DECIMAL)) AS [GLOBAL AVERAGE]
		FROM (
				SELECT COUNT(P.ClientId) AS [TICKET COUNT]
				FROM Purchase P
				GROUP BY P.ClientId
			) XX
	) X
GROUP BY BT.[CLIENT ID], BT.[BOUGHT TICKETS], X.[GLOBAL AVERAGE]
ORDER BY BT.[BOUGHT TICKETS] DESC

